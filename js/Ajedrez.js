var Ajedrez = (function() {

   var llenarTabla = function(pet){
   var divTab = document.getElementById("tablero");
   var respuesta = pet.response;
   var filas = respuesta.split("\n");
   var tabla = document.createElement("table");
   tabla.setAttribute("id","_tabla_juego")
   var cuerpo = document.createElement("tbody");
   cuerpo.setAttribute("id","_body_table");
   var letras = filas[0].split("|");
   var aux = 8;
   for(var j = 1;j < filas.length;j++){
     var celdas = filas[j].split("|");
     var fila = document.createElement("tr");
     for(var i = 0;i<celdas.length;i++){
       var celda = document.createElement("td");
       var cadena = ""+letras[i]+aux;
       celda.setAttribute("id", cadena);
       celda.innerHTML = celdas[i];
       fila.appendChild(celda);
     }
     cuerpo.appendChild(fila);
     aux--;
   }
   tabla.appendChild(cuerpo);
   divTab.appendChild(tabla);
  };

  var descargar = function(pet) {
    var tabla = document.getElementById("resultado");
    var url = "https://criollocuellargregorio.bitbucket.io/CSV/Ajedrez.csv";
    var xhr = new XMLHttpRequest();

    var msj = document.getElementById("mensaje");


    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        mensaje.textContent = " ";
        if (this.status === 200) {
          llenarTabla(xhr);
        } else {
            mensaje.textContent =mensaje.textContent = ""; "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
        }
      }
    };
    xhr.open('GET', url, true);
    xhr.send();
  };


  var _mostrar_tablero = function() {
    descargar();
  };

  var _actualizar_tablero = function() {
    var boton = document.getElementById("actualizar");

     boton.addEventListener("click",function(obj_evento){
       var tabla = document.getElementById("tablero")
       tabla.removeChild(tabla.lastChild);
       descargar();
   });
  };

var coordenadas = function(){
           var numeros = [8,7,6,5,4,3,2,1];
           var letras = ['a','b','c','d','e','f','g','h'];

           var filas = document.getElementsByTagName('tr');

           for(var i = 0; i<filas.length; i++){
               var numero = numeros[i];
             for(var j = 0; j< filas[0].children.length; j++){
                 var letra = letras[j];
                 var nombre = ""+letra+numero;

                 var celda2 = filas[i].children[j];

             }
           }
       }

  var _mover_pieza = function(obj_parametros) {
    coordenadas();
    var origen = obj_parametros.de;
    var destino = obj_parametros.a;
    var celda_origen2 = document.getElementById(origen);
    document.getElementById
    var celda_destino2 =   document.getElementById(destino);
    var pieza =  celda_origen2.textContent;
    celda_origen2.textContent = "∅";
    celda_destino2.textContent = pieza;
  };

  return {
    "mostrarTablero":_mostrar_tablero,
    "actualizar_tablero":_actualizar_tablero,
    "moverPieza":_mover_pieza

  }
})();
